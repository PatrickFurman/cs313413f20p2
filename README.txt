TestIterator Questions
1) Functionality is not changed when switching between linked list and ArrayList
2) The list.remove() function will only remove the first occurrence of the provided number
while the original code would have removed all occurrences of the number

TestList Questions
1) As with before, functionality is the same when using linked list as with ArrayList
2) list.remove(5) will remove the element in index position 5 from the list
3) list.remove(Integer.valueOf(5)) will remove the first occurrence of the number 5 from the list

TestPerformance Questions
With Size = 1,000:
    ArrayList was much quicker for accessing elements (ArrayListAccess took 11 ms while
    LinkedListAccess took 293 ms) but was also considerably slower for adding and removing elements
    (ArrayListAddRemove took 300 ms while LinkedListAddRemove took 20 ms)

With Size = 10,000:
    Above relationships still hold with the following speeds:
    ArrayListAccess 12 ms
    LinkedListAccess 3,520 ms
    ArrayListAddRemove 2,830 ms
    LinkedListAddRemove 21 ms

From this data it's clear that ArrayLists are more efficient for accessing data quickly but fall
short of LinkedLists if elements need to be added very frequently or in real-time